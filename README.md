# React Developer Candidate Assignment

---

React application that is going to act as a very simple stock portfolio tracker.
You can use this  
[API](https://www.alphavantage.co) for stock information

## App Overview

---

The application will be divided into 2 sections:
Dashboard where user can add companies stocks to the portfolio
Specific company details

## Stack

---

1.  [React](https://reactjs.org/)
1.  [Redux](https://redux.js.org/)
1.  [styled-component](https://styled-components.com/)
1.  [TypeScript](https://www.typescriptlang.org/)
1.  [MUI](https://mui.com/)

## Setup

- To run this project, install it locally using npm:

```
$ cd ../sdh-testtask
$ npm install
$ npm start
```
