import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import * as CompamiesActionCreators from "../store/action-creators/companies";

export const useActions = () => {
  const dispatch = useDispatch();
  return bindActionCreators(CompamiesActionCreators, dispatch);
};
