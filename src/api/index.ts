export const BASE_URL = "https://www.alphavantage.co/query";
export const API_SEARCH_URL = `${BASE_URL}?function=SYMBOL_SEARCH`;
export const API_DETAILS_URL = `${BASE_URL}?function=OVERVIEW`;
export const API_KEY = "DD011WWXCHBIHWDY";
