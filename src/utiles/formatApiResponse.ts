import { Companies, ICompany, IDetails } from "../types/companies";

// "1. dedfe".split(/\s(.+)/)[1];

const clean = (obj: any): any => {
  return Object.fromEntries(
    Object.entries(obj).map(([k, v]) => [k.split(/\s(.+)/)[1], v])
  );
};

export const cleanKeys = (arr: Companies): Companies => {
  const newArr = arr.map((item) => clean(item));
  console.log("newArr");
  console.log(newArr);
  return newArr;
};

export const unCapitallizeKeys = (item: IDetails): any => {
  return Object.fromEntries(
    Object.entries(item).map(([k, v]) => [
      k.charAt(0).toLowerCase() + k.slice(1),
      v,
    ])
  );
};
