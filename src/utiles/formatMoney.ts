export const formatMoney = (val: number) => {
  for (var e = 0; val >= 1000; e++) {
    val /= 1000;
  }
  return `${val.toFixed(3)} ${[" ", " k", " M", " G", " T", "P", "E"][e]}`;
};
