import React, { useEffect } from "react";
import { fetchDetails } from "../store/action-creators/companies";
import { useDispatch } from "react-redux";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { Typography, Grid, Box } from "@mui/material";
import { StyledButton } from "../components/PortfolioSection";
import DetailsCard from "../components/DetailsCard";
import { useNavigate } from "react-router-dom";
import Loader from "../components/shared/Loader";

export const Details = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { details, selectedCompany, isLoading } = useTypedSelector(
    (state) => state.companies
  );
  useEffect(() => {
    dispatch(fetchDetails(selectedCompany.symbol));
  }, [selectedCompany]);
  const handleNav = () => {
    console.log("back");
    navigate("/");
  };
  return (
    <Grid
      container
      spacing={2}
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <Box>
            <StyledButton variant="outlined" onClick={handleNav}>
              Back
            </StyledButton>
          </Box>
          <Box>
            {Object.keys(details).length === 0 ? (
              <Typography align="center">{`Sorry... There are no details about ${selectedCompany.name}...`}</Typography>
            ) : (
              <DetailsCard data={details} />
            )}
          </Box>
        </>
      )}
    </Grid>
  );
};

export default Details;
