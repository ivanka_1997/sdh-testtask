import React, { useEffect } from "react";
import { fetchCompanies } from "../store/action-creators/companies";
import { useDispatch } from "react-redux";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { ICompany, ActionTypes } from "../types/companies";
import { Typography, styled, Grid } from "@mui/material";
import Search from "../components/Search";
import CompaniesSection from "../components/CompaniesSection";
import { PortfolioSection } from "../components/PortfolioSection";
import Loader from "../components/shared/Loader";
import { isError } from "util";
import ErrorMessage from "../components/shared/ErrorMessage";
import { useNavigate } from "react-router-dom";

export const Dashboard = () => {
  const dispatch = useDispatch();

  const { companies, savedCompanies, isLoading, isError } = useTypedSelector(
    (state) => state.companies
  );
  const handleSearch = (query: string) => {
    dispatch(fetchCompanies(query));
  };
  const handleAddItem = (item: ICompany) => {
    dispatch({
      type: ActionTypes.ADD_TO_PROFILE,
      payload: item,
    });
  };
  const handleDeleteItem = (item: ICompany) => {
    dispatch({
      type: ActionTypes.DELETE_FROM_PROFILE,
      payload: item.name,
    });
  };
  if (isError) {
    return (
      <ErrorMessage message="Something went wrong. Please, refresh your brouser." />
    );
  }
  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <Grid container spacing={5}>
          <Grid item xs={12} sm={6}>
            <Search header="Company Name" onChange={handleSearch} />
            <CompaniesSection
              header="Search Results"
              data={companies}
              handleItemClick={handleAddItem}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <PortfolioSection
              header="Your Portfolio"
              handleItemClick={handleDeleteItem}
              data={savedCompanies}
            />
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default Dashboard;
