import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`

  *, *::before, *::after {
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    margin: 0;
    padding: 0;

  }

  
  body {
 

    font-family: "Roboto", sans-serif;
    display: flex;
    height: 100vh;
    width:100%;
  }
  #root{
    display: grid;
    gap: 20px;
  height:inherit;
  width: inherit;
    grid-template-columns: 1fr;
    grid-template-rows: 66px 1fr;
        grid-template-areas:
        "header"
        "content";
  }
`;

export default GlobalStyle;
