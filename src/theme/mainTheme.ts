export const theme = {
  yellow: "#FFD829",
  white: "#FFFFFF",
  lightGrey: "#E9E9E9",
  boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
  borderRadius: "10px",
  borderStyle: "1px solid #FFD829",
  medium: 500,
  fontSize: {
    xxs: "1.2rem",
    xs: "1.4rem",
    s: "1.6rem",
    m: "1.8rem",
  },
};
