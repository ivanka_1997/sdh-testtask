import React, { FC, ReactElement } from "react";
import { Box, Typography, styled, Grid } from "@mui/material";
import { StyledButton } from "../PortfolioSection";

type ErrorProps = {
  message: string;

  className?: string;
};

const ErrorMessage: FC<ErrorProps> = ({ message }): ReactElement => {
  return (
    <Box>
      <Typography>{message}</Typography>
    </Box>
  );
};

export default ErrorMessage;
