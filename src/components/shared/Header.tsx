import React, { ReactElement, FC } from "react";
import styled from "styled-components";

const Wrapper = styled.header`
  position: fixed;
  height: 66px;
  grid-area: header;
  width: 100%;
  left: 0px;
  top: 0px;
  background: white;
`;
const HeaderText = styled.h2`
  text-align: left;
  padding: 15px;
`;

const Header: FC<{ header: string }> = ({ header }): ReactElement => {
  return (
    <Wrapper>
      <HeaderText>{header}</HeaderText>
    </Wrapper>
  );
};

export default Header;
