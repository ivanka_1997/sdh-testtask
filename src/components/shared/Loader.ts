import styled from "styled-components";
import { keyframes } from "styled-components";

const rotateLoader = keyframes`
 from {
    transform: rotate(0);
  }
  to {
    transform: rotate(360deg);
  }`;

const Loader = styled.div`
  margin: auto;
  width: 50px;
  height: 50px;
  border: 5px solid ${({ theme }) => theme.lightGrey};
  border-left-color: ${({ theme }) => theme.yellow};
  border-radius: 50%;
  background: transparent;
  animation-name: ${rotateLoader};
  animation-iteration-count: infinite;
  animation-duration: 1s;
  animation-timing-function: linear;
  position: absolute;
  top: 50%;
  left: 50%;
`;
export default Loader;
