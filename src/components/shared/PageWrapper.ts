import styled from "styled-components";

const PageWrapper = styled.main`
  grid-area: content;
  display: grid;
  width: 95%;
  margin: 0 auto;
`;

export default PageWrapper;
