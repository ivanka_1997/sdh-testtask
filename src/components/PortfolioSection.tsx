import React, { FC, ReactElement } from "react";
import { useNavigate } from "react-router-dom";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Companies, ICompany, ActionTypes } from "../types/companies";
import { Box, Typography, Button, styled } from "@mui/material";
import { useDispatch } from "react-redux";

type PortfolioSectionProps = {
  header: string;
  data: Companies;
  handleItemClick: (item: ICompany) => void;
};

export const PortfolioSection: FC<PortfolioSectionProps> = ({
  header,
  data,
  handleItemClick,
}): ReactElement => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const setActiveCompany = (item: ICompany) => {
    console.log(item);
    dispatch({
      type: ActionTypes.SET_ACTIVE_COMPANY,
      payload: item,
    });
    navigate(`/details/${item.symbol.toLowerCase().replace(" ", "-")}`);
  };

  return (
    <Box>
      <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="h6">
        {header}
      </Typography>
      {data.length !== 0 ? (
        <StyledTable aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Name</TableCell>
              <TableCell align="center">Symbol</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((row) => (
              <TableRow
                key={row.name}
                data-name={row.name}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell
                  component="th"
                  scope="row"
                  align="left"
                  sx={{ cursor: "pointer" }}
                  onClick={() => setActiveCompany(row)}
                >
                  {row.name}
                </TableCell>
                <TableCell align="center">{row.symbol}</TableCell>
                <TableCell align="center">
                  <StyledButton
                    variant="outlined"
                    onClick={() => handleItemClick(row)}
                  >
                    Remove
                  </StyledButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </StyledTable>
      ) : (
        <Typography align="center">
          Looks like your portfolio is empty...
        </Typography>
      )}
    </Box>
  );
};

const StyledTable = styled(Table)`
  border-radius: 10px;
  border: solid 1px #ffd829;
  box-sizing: border-box;
  cursor: default;
  outline: none;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`;
export const StyledButton = styled(Button)`
  border: solid 1px #ffd829;
  color: black;
  box-shadow: none;
  &:hover,
  &:active {
    background-color: #ffd829;
    border-color: #ffd829;
    color: black;
  }
  &:focus {
    box-shadow: "0 0 0 0.2rem rgba(0,123,255,.5)";
  }
`;
