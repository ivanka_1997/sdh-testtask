import React, { FC, ReactElement } from "react";
import SerchInput from "../components/SerchInput";
import List from "@mui/material/List";
import { Typography, Box } from "@mui/material";

type SerchProps = {
  header: string;
  onChange: (val: string) => void;
};
export const Search: FC<SerchProps> = ({ header, onChange }): ReactElement => {
  return (
    <Box>
      <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="h6">
        {header}
      </Typography>
      <SerchInput onChange={onChange} prompt="Search..."></SerchInput>
    </Box>
  );
};

export default Search;
