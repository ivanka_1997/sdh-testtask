import React, { useEffect, FC, ReactElement } from "react";
import ListItemComponent from "./ListItemComponent";
import List from "@mui/material/List";
import { ICompany, Companies } from "../types/companies";
import { Typography, styled, Box } from "@mui/material";
import { generateKey } from "../utiles/generateKey";

type CompaniesSectionProps = {
  header: string;
  data: Companies;
  handleItemClick: (item: ICompany) => void;
};
export const CompaniesSection: FC<CompaniesSectionProps> = ({
  header,
  data,

  handleItemClick,
}): ReactElement => {
  return (
    <Box>
      <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="h6">
        {header}
      </Typography>
      {data.length !== 0 ? (
        <StyledList>
          {data.map((item: ICompany) => (
            <ListItemComponent
              key={generateKey(item.symbol)}
              item={item}
              handlerClick={handleItemClick}
            />
          ))}
        </StyledList>
      ) : (
        <Typography align="center">Empty results...</Typography>
      )}
    </Box>
  );
};

export default CompaniesSection;

const StyledList = styled(List)`
  border-radius: 10px;
  box-sizing: border-box;
  cursor: default;
  outline: none;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`;
