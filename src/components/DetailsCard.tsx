import React, { FC, ReactElement } from "react";
import { IDetails } from "../types/companies";
import { Box, Typography, styled } from "@mui/material";
import { formatMoney } from "../utiles/formatMoney";

type CardProps = {
  data: IDetails;
};
const StyledHeaders = styled(Typography)`
  display: inline;
  font-weight: 600;
`;

const DetailsCard: FC<CardProps> = ({ data }): ReactElement => {
  return (
    <Box sx={{ marginTop: "30px" }}>
      <Typography variant="h4" component="h4">
        {data.name || "no data"}
      </Typography>
      <Typography variant="body2">
        {" "}
        <StyledHeaders variant="subtitle1">Address:</StyledHeaders>
        {data.address || "no data"}
      </Typography>

      <Typography variant="body2">
        <StyledHeaders variant="subtitle1">
          Market Capitalization:
        </StyledHeaders>
        {data.marketCapitalization
          ? formatMoney(data.marketCapitalization)
          : "no data"}
      </Typography>
      <Typography sx={{ marginTop: "30px" }} variant="body2" component="p">
        {data.description || "no data"}
      </Typography>
    </Box>
  );
};

export default DetailsCard;
