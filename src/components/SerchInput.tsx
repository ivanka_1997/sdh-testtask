import React, { useState, useRef, useEffect, FC, ReactElement } from "react";
import styled from "styled-components";

type SearchProps = {
  prompt: string;
  onChange: (val: string) => void;
};
const SerchInput: FC<SearchProps> = ({ prompt, onChange }): ReactElement => {
  const [query, setQuery] = useState("");

  const handleInputValue = (val: string) => {
    setQuery(val);
    onChange(val);
  };
  return (
    <DropdownSearch>
      <input
        type="text"
        placeholder={prompt}
        value={query}
        onChange={(e) => {
          handleInputValue(e.target.value);
        }}
      />
    </DropdownSearch>
  );
};

export default SerchInput;

const DropdownSearch = styled.div`
  input {
    line-height: 1.5;
    font-size: 1.4rem;
    background-color: #fff;
    border-radius: 10px;
    border: solid 1px #ffd829;
    box-sizing: border-box;
    cursor: default;
    outline: none;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    padding: 8px 52px 8px 10px;
    transition: all 200ms ease;
    width: 100%;
  }
`;
