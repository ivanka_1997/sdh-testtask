import React, { useState, useRef, useEffect, FC, ReactElement } from "react";
import ListItemText from "@mui/material/ListItemText";
import IconButton from "@mui/material/IconButton";
import AddIcon from "@mui/icons-material/Add";
import { ICompany } from "../types/companies";
import { ListItem, styled } from "@mui/material";
import { ClassNames } from "@emotion/react";

export const StyledIconButton = styled(IconButton)`
  border: 1px solid #ffd829;
`;

type ListItemProps = {
  handlerClick: (item: ICompany) => void;
  item: ICompany;
};

const ListItemComponent: FC<ListItemProps> = ({
  item,
  handlerClick,
}): ReactElement => {
  return (
    <ListItem>
      <ListItemText primary={`${item.symbol} - ${item.name}`} />
      <StyledIconButton
        edge="end"
        aria-label="add"
        onClick={() => handlerClick(item)}
      >
        <AddIcon />
      </StyledIconButton>
    </ListItem>
  );
};

export default ListItemComponent;
