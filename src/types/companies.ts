export enum ActionTypes {
  FETCH_START = "FETCH_START",
  ADD_TO_PROFILE = "ADD_TO_PROFILE",
  DELETE_FROM_PROFILE = "DELETE_FROM_PROFILE",
  FETCH_SUCCESS = "FETCH_SUCCESS",
  FETCH_ERROR = "FETCH_ERROR",
  FETCH_COMPANIES_SUCCES = "FETCH_COMPANIES_SUCCESS",
  GET_DETAILS = "GET_DETAILS",
  FETCH_DETAILS_SUCCES = "FETCH_DETAILS_SUCCES",
  SET_ACTIVE_COMPANY = "SET_ACTIVE_COMPANY",
}

export interface State {
  companies: Companies;
  savedCompanies: Companies;
  selectedCompany: ICompany;
  details: IDetails;
  isLoading: boolean;
  isError: boolean;
}

export interface ICompany extends IObjectKeys {
  symbol: string;
  name: string;
}

export type Companies = ICompany[];
interface IObjectKeys {
  [key: string]: any;
}
// Company name
// Company address
// Company market capitalization (ideally format the raw data)
// Company description

export interface IDetails extends IObjectKeys, ICompany {
  description: string;
  adress: string;
}

interface FetchDataAction {
  type: ActionTypes.FETCH_START;
}
interface FetchDataErrorAction {
  type: ActionTypes.FETCH_ERROR;
}
interface AddFavoriteAction {
  type: ActionTypes.ADD_TO_PROFILE;
  payload: ICompany;
}
interface FetchCompaniesSuccessAction {
  type: ActionTypes.FETCH_COMPANIES_SUCCES;
  payload: Companies;
}

interface FetchDetailsSuccessAction {
  type: ActionTypes.FETCH_DETAILS_SUCCES;
  payload: IDetails;
}
interface DeleteFromProfileAction {
  type: ActionTypes.DELETE_FROM_PROFILE;
  payload: string;
}
interface SetActiveCompany {
  type: ActionTypes.SET_ACTIVE_COMPANY;
  payload: ICompany;
}
export type CompanyAction =
  | FetchDataAction
  | SetActiveCompany
  | DeleteFromProfileAction
  | FetchDataErrorAction
  | AddFavoriteAction
  | FetchDetailsSuccessAction
  | FetchCompaniesSuccessAction;
