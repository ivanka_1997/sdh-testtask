import { ActionTypes, CompanyAction, State } from "../../types/companies";

const initialState: State = {
  companies: [],
  savedCompanies: [],
  selectedCompany: {
    symbol: "",
    name: "",
  },
  details: {
    symbol: "",
    name: "",
    description: "",
    adress: "",
  },
  isLoading: false,
  isError: false,
};

export const companiesReducer = (
  state = initialState,
  action: CompanyAction
): State => {
  switch (action.type) {
    case ActionTypes.FETCH_START:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case ActionTypes.FETCH_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    case ActionTypes.FETCH_COMPANIES_SUCCES:
      return {
        ...state,
        isLoading: false,
        isError: false,
        companies: action.payload,
      };
    case ActionTypes.FETCH_DETAILS_SUCCES:
      return {
        ...state,
        isLoading: false,
        isError: false,
        details: action.payload,
      };
    case ActionTypes.ADD_TO_PROFILE:
      let filtredProfile = state.savedCompanies.filter(
        (item) => item.name === action.payload.name
      );
      if (filtredProfile.length > 0) {
        return state;
      }
      return {
        ...state,
        savedCompanies: [...state.savedCompanies, action.payload],
      };
    case ActionTypes.DELETE_FROM_PROFILE:
      return {
        ...state,
        savedCompanies: state.savedCompanies.filter(
          (item) => item.name !== action.payload
        ),
      };
    case ActionTypes.SET_ACTIVE_COMPANY:
      return {
        ...state,
        selectedCompany: action.payload,
      };
    default:
      return state;
  }
};
