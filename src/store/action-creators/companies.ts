import { Dispatch, compose } from "redux";
import {
  CompanyAction,
  ActionTypes,
  IDetails,
  Companies,
} from "../../types/companies";
import { API_DETAILS_URL, API_KEY, API_SEARCH_URL } from "../../api";
import { cleanKeys, unCapitallizeKeys } from "../../utiles/formatApiResponse";

const fetchCompanies = (query: string) => {
  return (dispatch: Dispatch<CompanyAction>) => {
    dispatch({ type: ActionTypes.FETCH_START });

    fetch(`${API_SEARCH_URL}&keywords=${query}&apikey=${API_KEY}`)
      .then((response) => response.json())
      .then((data) => {
        const formatetData = cleanKeys(data.bestMatches);
        dispatch({
          type: ActionTypes.FETCH_COMPANIES_SUCCES,
          payload: formatetData,
        });
      })
      .catch((err) =>
        dispatch(
          dispatch({
            type: ActionTypes.FETCH_ERROR,
          })
        )
      );
  };
};

const fetchDetails = (query: string) => {
  return (dispatch: Dispatch<CompanyAction>) => {
    dispatch({ type: ActionTypes.FETCH_START });

    fetch(`${API_DETAILS_URL}&symbol=${query}&apikey=${API_KEY}`)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        const formatetData = unCapitallizeKeys(data);
        dispatch({
          type: ActionTypes.FETCH_DETAILS_SUCCES,
          payload: formatetData,
        });
      })
      .catch((err) =>
        dispatch(
          dispatch({
            type: ActionTypes.FETCH_ERROR,
          })
        )
      );
  };
};

export { fetchDetails, fetchCompanies };
