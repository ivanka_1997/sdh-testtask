import React, { Suspense } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Header from "./components/shared/Header";
// import { theme } from "./theme/mainTheme";
// import { GlobalStyle } from "./theme/GlobalStyle";
import Details from "./views/Details";
// import { ThemeProvider } from "styled-components";
// import Loader from "./components/shared/Loader";
import Dashboard from "./views/Dashboard";
import GlobalStyle from "./theme/GlobalStyle";
import { ThemeProvider } from "styled-components";
import { theme } from "./theme/mainTheme";
import PageWrapper from "./components/shared/PageWrapper";

function App() {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Header header="SDH Frontend Task" />
        {/* <Suspense fallback={<Loader />}> */}
        <PageWrapper>
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<Dashboard />} />
              <Route path="/details/:companyName" element={<Details />} />
            </Routes>
          </BrowserRouter>
        </PageWrapper>
        {/* </Suspense> */}
      </ThemeProvider>
    </>
  );
}

export default App;
